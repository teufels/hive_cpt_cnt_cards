
plugin.tx_hivecptcntcards_hivecptcntcardscardrendercard {
    view {
        # cat=plugin.tx_hivecptcntcards_hivecptcntcardscardrendercard/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_cards/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntcards_hivecptcntcardscardrendercard/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_cards/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntcards_hivecptcntcardscardrendercard/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_cards/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntcards_hivecptcntcardscardrendercard//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder